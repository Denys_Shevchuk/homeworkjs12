let myInterval;
let images = ["img/2.jpg", "img/3.JPG", "img/4.png", "img/1.jpg"], x = -1;
function setImages() {
    x = (x === images.length - 1) ? 0 : x + 1;
    document.getElementById("img").src = images[x];
}
myInterval = setInterval(setImages, 10000);

document.getElementById('stop').addEventListener('click', function () {
    clearInterval(myInterval);
});
document.getElementById('start').addEventListener('click', function () {
    myInterval = setInterval(setImages, 10000);
});